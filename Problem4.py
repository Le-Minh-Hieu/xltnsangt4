"""==============Problem4=============="""

# a)Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
print("a)")
t=(1,'python',[2,3],(4,5))
print(t)
# b)Unpack t into 6 variables, that are: 1, 'python', 2, 3, 4, 5
print("b)")
print("Giai nen t ra 6 bien")
a,b,x,y=(1,'python',[2,3],(4,5))
print(a)
print(b)
print(x[0])
print(x[1])
print(y[0])
print(y[1])
# c)Print out the last element of t
print("c)")
print(t[3])

# d)Add t to a list [2, 3]
print("d)")
v=[2,3]
v.append(t)
print(v)
# e)Check whether list [2, 3] is duplicated in t
print("e)")
print([2,3] in t)
# f)Remove list [2, 3] from t
print("f)")
print("Can't remove")
# g)Convert tuple t into a list
print("g)")
print(list(t))