"""==============Problem5=============="""
# a)Write a Python script to concatenate following dictionaries to create a new one.
print("a)")
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
dic={}
dic.update(dic1)
dic.update(dic2)
dic.update(dic3)
print("dic=" ,dic)
"""Write a Python script to print a dictionary where the keys are numbers
between 1 and 15 (both included)
and the values are square of keys"""
i=1
square={1:1}
while(i<=15):
    square[i]=i*i
    i+=1
print("square=", square)
# b. Write a Python script to sort ascending a dictionary by value. For example,
print("b)")
dicx={'a':6,'b':3,'c':2,'d':4,'e':9}
print("dic=",dicx)
import operator
d=sorted(dicx.items(),key=operator.itemgetter(1))
p,q,r,s,t=d
print("output=")
print(p[0])
print(q[0])
print(r[0])
print(s[0])
print(t[0])
"""c)Create a dictionary where the keys are unique characters of the
string and the mapped values are their occurrence in the string"""
print("c)")
dict={}
str1="Python is an easy language to learn"
for key in range (len(str1)):
    dict[str(str1[key])]=str1.count(str(str1[key]))
print(dict)
