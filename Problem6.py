"""=============Problem6-String============="""
# a)Get a string from console like this: s = 3 4, unpack s to 2 integer number row, col
print("a)")
s=input()
print("type s= ",type(s))

row=int(s[0])
print("row= ", row)
col=int(s[1])
print("col= ", col)

# b)Randomly generate a list l that has row values and each value of l is a
# list with col values.
print("b)")
l=[]
import random
l1 = []
for j in range(row):
    l.append(l1)
    for i in range(col):
        r=random.randint(0,9)
        l1.append(r)
    l1=[]
print("\n  ",l[0],"\nl=",l[1],"\n  ",l[2])
