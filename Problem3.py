"""==============Problem3=============="""
# a)Create three sets: A = {1, 2, 3, 4, 5, 7}, B = {2, 4, 5, 9, 12, 24}, C = {2, 4, 8}
print("a)")
A={1,2,3,4,5,7}
B={2,4,5,9,12,24}
C={2,4,8}
print("A=",A)
print("B=",B)
print("C=",C)
# b)Iterate over all elements of set C and add each element to A and B
print("b)")
print("Items of C: ")
for n in C:
    print(n)
    A.add(n)
    B.add(n)

# c)Print out A and B after adding elements
print("c)")
print("new A=",A)
print("new B=",B)

# d)Print out the intersection of A and B
print("d)")
print("A&B: ",A&B)

# e)Print out the union of A and B
print("e)")
print("A|B: ",A|B)

# f)Print out elements in A but not in B
print("f)")
print("A-B: ",A-B)

# g)Print out the length of A and B
print("g)")
print("lenght A=",len(A))
print("lenght B=",len(B))

# h)Print out the maximum value of A union B
print("h)")
print("MAXIMUM A|B= ",max(A|B))

# i)Print out the minimum value of A union B
print("i)")
print("MINIMUM A|B=", min(A|B))