"""==============Problem1=============="""
s="Hi john, welcome to python programming for beginner";
#a) Check a string “python” that either exists in s or not.
print("a)")
print("python" in s)
#b) Extract the word “john” from s and save it into a variable named s1.
print("b)")
s1=s[3:7]
s=s.replace(s1,"")
print(s)
print("s1=",s1)
# c) Count how many character ‘o’ in s and print it on console. Guide: use count() function of string
print("c)")
print(s.count('o'))
# d. Count how many word in s and print it on console.
print("d)")
print(len(s.split()))
