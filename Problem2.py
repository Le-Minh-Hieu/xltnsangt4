"""==============Problem2=============="""
print("a)")
# a)Remove the item “python” in the list.
l=[23, 4.3, 4.2, 31,'python', 1, 5.3, 9, 1.7]
l.remove('python')
print(l)

print("b)")
# b)Sort this list by ascending and descending.
l.sort()
print(l)
l.sort(reverse=1)
print(l)

print("c)")
# c)Check either number 4.2 to be in l or not?
print(4.2 in l)